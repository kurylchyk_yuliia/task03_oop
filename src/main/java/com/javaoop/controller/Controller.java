package com.javaoop.controller;

import  com.javaoop.model.Model;
import com.javaoop.view.View;
public class Controller {

    private int chosenOption;
    private Model model = new Model();
    private View view  = new View();

    public Controller(){
       chosenOption =  ourGood();
       switch(chosenOption)
       {

           case 1: checkedHomeFlower();
                break;
           case 2: checkedCommonFlower();
                break;

       }
    }

    public void checkedHomeFlower(){
        model.setHomeFlowerAndPot();
        view.showOrder(model.getHomeFlowerAndPot(),model.getHomePrice());

    }

    public void checkedCommonFlower() {

         model.setCustomBouquet();
         model.getBouquet();
         view.showOrder(model.getBouquet(),model.getBouquetPrice());
        }


    int ourGood(){
        int answer;
       answer =  view.getOurGoods();
       switch (answer)  {

           case 1:
               view.ShowInformation(model.showHomeFlower(),model.showPot());
               break;
           case 2:
               view.ShowInformation(model.showCommonFlower());
               break;

       }

        return answer;

    }

}
