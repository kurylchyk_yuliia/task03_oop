package com.javaoop.view;

import com.sun.xml.internal.bind.v2.runtime.output.SAXOutput;

import java.util.Scanner;
import java.util.LinkedHashMap;

public class View {

    Scanner sc;

    public View() {
        sc = new Scanner(System.in);
    }

    public int getOurGoods() {

        int answer = 0;
        System.out.println("Hello! What are you looking for? :");
        System.out.println("1 - home flowers");
        System.out.println("2 - common flowers");

        while(answer!=1 && answer!=2){
            System.out.print("Choose the option: ");
                answer = sc.nextInt();

        }

        return answer;
    }

    public void ShowInformation(String homeFlower, String pot){
        System.out.println(homeFlower);
        System.out.println(pot);
    }
    public void ShowInformation(String commonFlower)
    {
        System.out.println(commonFlower);
    }


    public void showOrder(String[] order,double price)
    {
        System.out.println("Your bouquet consists of ");
        for(int index = 0; index<order.length; index++)
        {
            System.out.println(order[index]);
        }
        System.out.println("Sum  = " + price );

    }

    public void showOrder(String order,double price)
    {
        System.out.println("Your order: ");
        System.out.println(order);
        System.out.println("The sum is " +price);
    }
}
