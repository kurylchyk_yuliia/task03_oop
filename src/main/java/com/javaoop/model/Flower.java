package com.javaoop.model;

public abstract class Flower {

  protected   double price;
  protected   String name;
  protected   String color;

  public Flower(String name, String color, double price){

        this.name = name;
        this.color = color;
        this.price = price;
    }

    public double getPrice() {

        return price;
    }
    public  String getName(){
        return name;
    }
    public  String getColor(){
        return color;
    }
    public abstract String toString();

}
