package com.javaoop.model.common;

import com.javaoop.model.common.*;
import java.io.*;
import java.util.Scanner;

public class Bouquet {

    private int numberOfFlowers;
    private CommonFlower[] bouquet;
    private int indexOfFlower = 0;


    public int getNumberOfFlowers() {
        return numberOfFlowers;
    }

    public  Bouquet() {
      setNumberOfFlowers();
      bouquet = new CommonFlower[numberOfFlowers];
  }

    public CommonFlower[]  getBouquet()
    {
        return bouquet;

    }

    public void createBouquet() {

        int flower = 0;
        int count = 0;
        Scanner sc = new Scanner(System.in);
        while (indexOfFlower < bouquet.length) {

            while(flower<=0||flower>7) {
                System.out.print("Choose the flower(1-7): ");
                flower = sc.nextInt();
            }

            while(count>bouquet.length||(count+indexOfFlower)>bouquet.length ||count <=0) {
                System.out.println("And number(not bigger than " + (bouquet.length - indexOfFlower) + ")");
                count = sc.nextInt();
            }

            chooseFlower(flower, count);
            indexOfFlower += count;

            flower = 0;
            count = 0;
        }

    }

    public CommonFlower getFlowerFromBouquet(int index)
    {

        try {
            return bouquet[index];
        }catch(ArrayIndexOutOfBoundsException ex){
            System.out.println("There is no element like that");
        }

        return null;
    }

    private void chooseFlower(int flower, int count) {

        int indexOfArray = indexOfFlower;
        flower--;



        int lineInFile = 0;
        try {

            RandomAccessFile raf = new RandomAccessFile("flower.txt", "r");



            while (lineInFile != flower ||(flower==0 &&flower==lineInFile)) {

                if(flower!=0) {
                    raf.readLine();
                    lineInFile++;}
                if (lineInFile == flower) {

                    String arr[] = {"","","",""};

                    //for name,breed, color and price(String)

                    double price = 0.0d;
                    try {

                        for(int index = 0; index<arr.length; index++){
                            char c = '\u0000';
                            while (c != ' ') {
                                c = (char) raf.read();
                                if (c == ' ' || c=='\n') {
                                    break;
                                }

                                arr[index] += c;
                            }

                        }price = Double.parseDouble(arr[3]);
                    } catch (IOException exc) {
                        System.out.println("Troubles with file");
                    }

                    switch (flower) {
                        case 0:
                        case 1:
                        case 2:
                            boolean needle = Rose.setNeedle();
                            int lengthOfStem = Rose.setLengthOfStem();
                            for (int index = 0; index < count; index++) {
                                bouquet[indexOfArray] = new Rose(arr[0], arr[2], arr[1], price,needle,lengthOfStem);
                                indexOfArray++;
                            }
                            if(flower ==0 && lineInFile==0) {
                                lineInFile = flower = 1;
                            }
                            System.out.println("Rose are added!");
                            break;
                        case 3:
                        case 4:
                            for (int index = 0; index < count; index++) {

                                bouquet[indexOfArray] = new Tulip(arr[0], arr[2], arr[1],price);
                                indexOfArray++;
                            }

                            if(flower ==0 && lineInFile==0) {
                                lineInFile = flower = 1;
                            }
                            System.out.println("Tulips are added!");
                            break;
                        case 5:
                        case 6:
                            for (int index = 0; index < count; index++) {

                                bouquet[indexOfArray] = new Lilium(arr[0], arr[2], arr[1],price);
                                indexOfArray++;
                            }
                            if(flower ==0 && lineInFile==0) {
                                lineInFile = flower = 1;
                            }
                            System.out.println("Liliums are added!");
                            break;

                        default:
                            if(flower ==0 && lineInFile==0) {
                                lineInFile = flower = 1;
                            }
                            System.out.println("No option like that");



                    }

                }

            }

        } catch (FileNotFoundException ex) {
            System.out.println("File is not found");
        } catch (IOException ex) {
            System.out.println("Troubles with file");
        }


    }

    private void setNumberOfFlowers() {


          int size = 0;
          Scanner sc = new Scanner(System.in);
          while (size <= 0) {
              System.out.println("Enter the number of flowers in bouquet");
              size = sc.nextInt();
          }
        numberOfFlowers = size;
    }

}
