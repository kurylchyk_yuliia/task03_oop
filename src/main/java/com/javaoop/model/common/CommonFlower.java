package com.javaoop.model.common;

import com.javaoop.model.Flower;
public abstract class CommonFlower extends Flower {

    protected  String breed;
    CommonFlower(String name, String color,String breed,double price)
    {

        super(name,color,price);
        this.breed = breed;
    }

    public String getBreed() {
        return breed;
    }

    @Override
    public String toString(){

        return name + "\t" + breed + "\t" + color + "\t = " +price;
    }


}
