package com.javaoop.model.common;

import java.io.File;
import java.util.Scanner;

public class Rose extends CommonFlower {
    private static Scanner sc = new Scanner(System.in);
    private boolean needle;
    private int lengthOfStem;

    Rose(String name, String color, String breed, double price, boolean needle, int lengthOfStem) {
        super(name, color, breed, price);

        this.needle = needle;
        this.lengthOfStem = lengthOfStem;

    }

    @Override
    public String toString() {
        return super.toString() + "\t" + (needle ? "with" : "without") + "\tneedles\t" + "with " + lengthOfStem + "\t cm of stem";
    }

    public static boolean setNeedle() {

        System.out.print("Cut off the needles (yes/no): ");
        String line = sc.nextLine();
        return (line == "yes" || line == "y") ? true : false;
    }

    public static int setLengthOfStem() {
        int length = 0;
        while (length <= 0 || length > 100) {
            System.out.println("Enter the length, which you want: ");
            length = sc.nextInt();
        }
        return length;
    }


}

