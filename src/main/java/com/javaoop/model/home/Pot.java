package com.javaoop.model.home;

public class Pot {

    private String size;
    private String color;
    private String shape;

    public String getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public String getShape() {
        return shape;
    }
    public String toString()
    {
        return size + "\t" +shape + "\t" +color;
    }

    public Pot(String size, String shape,String color) {
        this.size  =size;
        this.shape = shape;
        this.color = color;
    }

}
