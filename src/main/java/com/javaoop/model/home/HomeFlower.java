package com.javaoop.model.home;

import com.javaoop.model.Flower;

public class HomeFlower extends Flower {

   private Pot pot;


   public HomeFlower(String name,String color, double price, Pot pot) {
        super(name,color,price);
        this.pot  = pot;
    }

    @Override
    public String toString() {
        return name + "\t" + color + "\tin\t"+ pot.toString() +"\tpot"+ "\t = " + price;

    }




}
