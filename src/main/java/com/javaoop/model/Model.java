package com.javaoop.model;
import com.javaoop.model.common.CommonFlower;
import com.javaoop.model.home.HomeFlower;
import com.javaoop.model.common.Bouquet;
import com.javaoop.model.home.Pot;

import javax.activation.CommandInfo;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class Model {


    Bouquet bouquet;
    HomeFlower homeFlower;


public String getHomeFlowerAndPot(){
    return homeFlower.toString();
}


    public String showHomeFlower() {

        System.out.println("Available home flowers: ");
        String homeFlowers = "";
        try {

            File file = new File("homeFlower.txt");

            Scanner input = new Scanner(file);

            int index = 1;

            while (input.hasNextLine()) {

                String line = input.nextLine();
                homeFlowers+=index+")"+ line +"\n";
                index++;
            }
            input.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return homeFlowers;
    }

    public String showPot(){

        System.out.println("Available pots: ");
        String potInfo ="";
        try{
            File file = new File("pot.txt");
            Scanner input = new Scanner(file);

            int index = 1;

            while (input.hasNextLine()) {

                String line = input.nextLine();
                potInfo+=index+")"+ line +"\n";
                index++;
            }
            input.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return potInfo;
    }

public double getHomePrice(){
    return homeFlower.getPrice();
}
public double getBouquetPrice(){
    double price = 0.0;
    for(int index = 0; index< bouquet.getNumberOfFlowers(); index++) {
        price += (bouquet.getFlowerFromBouquet(index)).getPrice();
    }
    return price;



}

    public void setHomeFlowerAndPot() {
        int flower = 0;
        int pot = 0;
        Scanner sc = new Scanner(System.in);

        while (flower <= 0 || flower > 4){
            System.out.print("Choose the flower (1-4): ");
              flower = sc.nextInt();
            }
        while(pot<=0 || pot>3) {
            System.out.print("Choose the pot(1-3): ");
            pot = sc.nextInt();
        }
        chooseHomeFlower(flower,pot);
    }

    private void chooseHomeFlower(int flower, int pot) {
        flower--;
        pot--;

        int lineInFile = 0;
        String arr[] = {"", "", ""};
        double price = 0.0d;
        try {
            RandomAccessFile raf = new RandomAccessFile("homeFlower.txt", "r");


            while (lineInFile != flower ||(flower==0 &&flower==lineInFile)) {

                if(flower!=0) {
                    raf.readLine();
                    lineInFile++;}

                if (lineInFile == flower) {


                    try {

                        for (int index = 0; index < arr.length; index++) {
                            char c = '\u0000';
                            while (c != ' ') {
                                c = (char) raf.read();
                                if (c == ' ' || c == '\n') {
                                    break;
                                }
                                arr[index] += c;
                            }

                        }
                        price = Double.parseDouble(arr[2]);
                        if(flower ==0 && lineInFile==0) {
                            lineInFile = flower = 1;
                        }

                    } catch (IOException exc) {
                        System.out.println("Troubles with file");
                    }
                }
            }
        } catch (IOException exc) {
            System.out.println("Troubles with file");
        }

        homeFlower= new HomeFlower(arr[0],arr[1],price,choosePot(pot));
    }

    private  Pot choosePot(int pot){
        int lineInFile = 0;
        String arr[] = {"", "", ""};
        try {
            RandomAccessFile raf = new RandomAccessFile("pot.txt", "r");

            while (lineInFile != pot ||(pot==0 &&pot==lineInFile)) {

                if(pot!=0) {
                    raf.readLine();
                    lineInFile++;}
                if (lineInFile == pot) {

                    try {

                        for (int index = 0; index < arr.length; index++) {
                            char c = '\u0000';
                            while (c != ' ') {
                                c = (char) raf.read();
                                if (c == ' ' || c == '\r' || c=='\n') {
                                    break;
                                }
                                arr[index] += c;
                            }

                        }

                        if(pot ==0 && lineInFile==0) {
                            lineInFile = pot = 1;
                        }
                    } catch (IOException exc) {
                        System.out.println("Troubles with file");
                    }
                }
            }
        } catch (IOException exc) {
            System.out.println("Troubles with file");
        }


        return new Pot(arr[0],arr[1],arr[2]);

    }


   //------------------------------FOR COMMON--------------------------

    public String showCommonFlower() {

       String availableFlower = "";
       try {

           File file = new File("flower.txt");

           Scanner input = new Scanner(file);

           int index = 1;

           while (input.hasNextLine()) {

               String line = input.nextLine();
               availableFlower+=index+")"+ line +"\n";
               index++;
           }
           input.close();

       } catch (Exception ex) {
           ex.printStackTrace();
       }

       return availableFlower;
   }


    public void setCustomBouquet() {

        bouquet = new Bouquet();
        bouquet.createBouquet();
    }

    private void sortBouquet(CommonFlower[] arr) {

        for (int index = 0; index < arr.length; index++) {

            for (int currentIndex = index + 1; currentIndex < arr.length; currentIndex++) {
                if (arr[index ].getPrice() > arr[currentIndex].getPrice()) {
                    CommonFlower temp = arr[index];
                    arr[index] = arr[currentIndex];
                    arr[currentIndex] = temp;
                }
            }
        }
    }


    public String[] getBouquet(){
        CommonFlower[] arr = bouquet.getBouquet();
        sortBouquet(arr);

        String[] arrStr = new String[arr.length];
        for(int index = 0; index<arr.length;index++)
        {
            arrStr[index] = arr[index].toString();
        }

        return arrStr;

    }

}
